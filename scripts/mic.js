var micRecorder;
var isMicRecording = 0;
var hasMicPermission = 1;
var inputPoint;
var micStream;

function testStream(stream) {
    $("#recordMicButton").show();
    stream.getAudioTracks()[0].stop();
    inputPoint = ctx.createGain();

    micRecorder = new Recorder(ctx);
    micRecorder.addInput(inputPoint);

    zeroGain = ctx.createGain();
    zeroGain.gain.value = 0.0;
    inputPoint.connect(zeroGain);
    zeroGain.connect(ctx.destination);
}

function initAudio() {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    navigator.getUserMedia({"audio": true}, testStream, function(e) {
        hasMicPermission = 0;
        $("#recordMicButton").hide();
    });
}

function recordMic() {
    if(isMicRecording) {
        $("#micRecordIcon").animate({
            "border-radius": "+=50%"
        });
        micRecorder.stop();
        micRecorder.exportWAV(micWav);
        isMicRecording = 0;
    } else {
        if(!inputPoint) { return; }
        $("#micRecordIcon").animate({
            "border-radius": "-=50%"
        });
        navigator.getUserMedia({"audio": true}, gotStream, function(e) {
            hasMicPermission = 0;
            $("#recordMicButton").hide();
        });
        isMicRecording = 1;
    }
}

function gotStream(stream) {
    var audioInput = ctx.createMediaStreamSource(stream);
    audioInput.connect(inputPoint);
    micRecorder.record();
    micStream = stream;
}

function micWav(wav) {
    var fileReader = new FileReader();
    fileReader.onload = function() {
        var arrayBuffer = this.result;
        
        idb.transaction("sounds", "readwrite").objectStore("sounds").add(arrayBuffer).onsuccess = function(e) {
            var fileId = e.target.result;
            $("#sourceSelect").val("file:" + fileId + "/recording");
            filesToUnload.push(fileId);
            micRecorder.clear();
            micStream.getAudioTracks().forEach(function(track) {
                track.stop();
            });
            inputChange(false);
        };
    };
    fileReader.readAsArrayBuffer(wav);
}

window.addEventListener('load', initAudio);
