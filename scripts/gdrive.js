var clientId = "689011528121-ofrseatjktgk9r26nsmrfqueipfa4cvb.apps.googleusercontent.com";
var appId = "689011528121";

var scope = ['https://www.googleapis.com/auth/drive'];

var supportedMimes = "video/webm,audio/webm,audio/ogg,video/ogg,application/ogg,audio/mpeg,audio/wave,audio/wav,audio/x-wav,audio/x-pn-wav,audio/flac,audio/x-flac,audio/mp3,video/mp4";

var pickerApiLoaded = false;
var oauthToken;

var loadQueue = [];

function gdSignIn() {
    if(oauthToken !== undefined) {
        var http = new XMLHttpRequest();
        http.open("GET", "https://accounts.google.com/o/oauth2/revoke?token=" + oauthToken, true);
        http.send(null);
        oauthToken = undefined;
        $("#gdSignIn > span").html("Sign in");
    } else {
        gapi.load('auth', {'callback': onAuthApiLoad});
        gapi.load('picker', {'callback': onPickerApiLoad});
    }
}

function onAuthApiLoad() {
    window.gapi.auth.authorize(
        {
            'client_id': clientId,
            'scope': scope,
            'immediate': false
        },
        handleAuthResult);
}

function onPickerApiLoad() {
    pickerApiLoaded = true;
}

function handleAuthResult(authResult) {
    if(authResult && !authResult.error) {
        oauthToken = authResult.access_token;
        $("#gdSignIn > span").html("Sign out");
        for(var loadKey in loadQueue) {
            gdReloadAudio(loadQueue[loadKey]);
        }
        loadQueue = [];
    }
}

function selectGdFile() {
    if(pickerApiLoaded && oauthToken) {
        var view = new google.picker.View(google.picker.ViewId.DOCS);
        view.setMimeTypes(supportedMimes);
        var picker = new google.picker.PickerBuilder()
            .enableFeature(google.picker.Feature.NAV_HIDDEN)
            .setAppId(appId)
            .setOAuthToken(oauthToken)
            .addView(view)
            .addView(new google.picker.DocsUploadView())
            .setCallback(pickerCallback)
            .build();
        picker.setVisible(true);
    } else {
        gdSignIn();
    }
}

function pickerCallback(data) {
    if(data.action == google.picker.Action.PICKED) {
        bindings[curSettings].source = 'gd:' + data.docs[0].id + '/' + data.docs[0].name;
        $("#sourceSelect").val(bindings[curSettings].source);
    }
}

function initialSignIn() {
    window.gapi.auth.authorize(
        {
            'client_id': clientId,
            'scope': scope,
            'immediate': true
        },
        handleAuthResult);
}

function loadGapi() {
    gapi.load('auth', {'callback': initialSignIn});
    gapi.load('picker', {'callback': onPickerApiLoad});
}

function gdReloadAudio(charNum) {
    setError(charNum, 1);
    updateReloadButton(true);
    if(oauthToken === undefined) {
        loadQueue.push(charNum);
        return;
    }
    bindings[charNum].audio = undefined;
    var fileId = bindings[charNum].source.substr(3).split("/")[0];
    var getFile = new XMLHttpRequest();
    getFile.onreadystatechange = function() { 
        if(getFile.readyState == 4 && getFile.status == 200) {
            ctx.decodeAudioData(getFile.response, function(buffer) {
                if(bindings[charNum] === undefined) {
                    return;
                }
                bindings[charNum].audio = buffer;
                showWaveform();
                calculateSync(bindings[charNum].audio, bindings[charNum].mode == "song");
                setError(charNum, 0);
                updateReloadButton(false);
            }, function() {
                loadError(charNum, bindings[charNum].source.split("/").slice(-1)[0]);
                updateReloadButton(false);
            });
        }
    };
    getFile.open("GET", "https://www.googleapis.com/drive/v3/files/" + fileId + '?alt=media', true);
    getFile.setRequestHeader('Authorization','Bearer ' + oauthToken);
    getFile.responseType = 'arraybuffer';
    getFile.send(null);
}
