var midiInputs = [];
var currentInput;
function reloadMidiList() {
    midiInputs = [];
    if(navigator.requestMIDIAccess) {
        navigator.requestMIDIAccess().then(function(midi) {
            var desired = localStorage.getItem("midiInput");
            var i = midi.inputs.values();
            var selectInd = 0;
            $("#midiInputs").html("");
            var ind = 0;
            for(var x = i.next(); x && !x.done; x = i.next()) {
                $("#midiInputs").append($("<option></option>").html(x.value.name));
                if(x.value.name == desired) {
                    selectInd = ind;
                }
                ind += 1;
                midiInputs.push(x.value);
            }
            if($("#midiInputs").html() === "") {
                $("#midiInputs").append($("<option></option>").html("No inputs found"));
            }
            $($("#midiInputs").children()[selectInd]).attr("selected", "selected");
            connectMidi();
        }, function(err){
            $("#midiInputs").html("");
            $("#midiInputs").append($("<option></option>").html("No inputs found"));
        });
    }
}
reloadMidiList();

function connectInput(input) {
    if(currentInput) {
        currentInput.onmidimessage = null;
    }
    currentInput = input;
    input.onmidimessage = function(ev) {
        var cmd = ev.data[0] >> 4;
        var channel = ev.data[0] & 0xf;
        var noteNumber = ev.data[1];
        var velocity = ev.data[2];

        if(channel == 9) {
            return;
        }
        if(cmd == 8 || ((cmd == 9) && (velocity === 0))) {
            midiKeyUp(256 + noteNumber, velocity);
        } else if(cmd == 9) {
            midiKeyDown(256 + noteNumber, velocity);
        }
    };
}

function connectMidi() {
    if(midiInputs.length === 0) {
        return;
    }
    var index = $("#midiInputs option:selected").index();
    if(index == -1) {
        index = 0;
    }
    connectInput(midiInputs[$("#midiInputs option:selected").index()]);
}

function midiKeyDown(key, velocity) {
    keyPressed(key, velocity / 100, 1);
}
function midiKeyUp(key, velocity) {
    if(mode != "play") {
        return;
    }
    if(bindings[key] !== undefined) {
        keyReleased(key);
    }
}

function getNoteName(midiCode) {
    var note = notenames[midiCode % 12];
    var octave = Math.floor(midiCode / 12) - 1;
    return note + "<sub>" + octave + "</sub>";
}
