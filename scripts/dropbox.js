var dbClient = new Dropbox.Client({ key: '8u4puoza6pcwulf' });
var dbSettings = 0;
var dbPath = "/";
var clickFile = 0;
dbClient.authenticate({ interactive: false }, function(error, client) {
});

$("body").ready(function() {
    if(dbClient.isAuthenticated()) {
        $("#dbSignIn > span").html("Sign out");
    }
    $("#dropboxSelect").hide();
});

function dbSignIn() {
    if(dbClient.isAuthenticated()) {
        dbClient.signOut({}, function(error) {
            $("#dbSignIn > span").html("Sign in");
        });
    } else {
        dbClient.authenticate(function(error, client) {
            if(error) {
                alert('There was an error while authenticating');
            } else {
                $("#dbSignIn > span").html("Sign out");
            }
        });
    }
}

function selectDbFile() {
    $("#settings").hide();
    $("#dropboxSelect").show();
    dbSettings = 1;
    viewDbDir();
}

function dbSettingsCancel() {
    if(clickFile) {
        clickFile = 0;
        return;
    }
    $("#settings").show();
    $("#dropboxSelect").hide();
    dbSettings = 0;
}

function viewDbDir() {
    dbClient.readdir(dbPath, {}, function(error, files, info) {
        if(error) {
        } else {
            if(info.isFile) {
                loadDbFile();
                topDir();
                return;
            }
            $("#dropboxFiles").html("");
            if(dbPath != "/") {
                newFiles = ['..'];
                newFiles.push.apply(newFiles, files);
                files = newFiles;
            }
            for(var i in files) {
                file = files[i];
                var link = $('<a href="#" onclick="clickDbFile(\'' + file + '\')">' + file + '</a>');
                var col = $('<td></td>');
                col.append(link);
                var row = $('<tr></tr>');
                row.append(col);
                $("#dropboxFiles").append(row);
            }
        }
    });
}

function clickDbFile(filename) {
    clickFile = 1;
    if(filename == "..") {
        topDir();
    } else {
        dbPath += filename + "/";
    }
    viewDbDir();
}

function topDir() {
    var parts = dbPath.substr(1, dbPath.length - 2).split('/');
    dbPath = '/';
    for(var i = 0; i < parts.length - 1; i++) {
        dbPath += parts[i] + '/';
    }
}

function loadDbFile() {
    bindings[curSettings].source = 'db:' + dbPath.substr(0, dbPath.length - 1);
    $("#sourceSelect").val(bindings[curSettings].source);
    dbSettingsCancel();
}

function dbReloadAudio(charNum) {
    dbClient.authenticate({interactive: false}, function(error, client) {
        setError(charNum, 1);
        updateReloadButton(true);
        if(error) {
            return;
        }
        bindings[charNum].audio = undefined;
        var fileName = bindings[charNum].source.substr(3);
        dbClient.readFile(fileName, {arrayBuffer: 1, httpCache: 1}, function(error, content, stat, rangeInfo) {
            if(error) {
                return;
            }
            ctx.decodeAudioData(content, function(buffer) {
                if(bindings[charNum] === undefined) {
                    return;
                }
                bindings[charNum].audio = buffer;
                showWaveform();
                calculateSync(bindings[charNum].audio, bindings[charNum].mode == "song");
                setError(charNum, 0);
                updateReloadButton(false);
            }, function() {
                loadError(charNum, fileName.split("/").slice(1).join("/"));
                updateReloadButton(false);
            });
        });
    });
}
