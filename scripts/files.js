var idb;
var filesToUnload = [];
var loadQueue = [];
$("#uploadFile").hide();
$("#recordMicButton").hide();
$("#fileUploadingWrapper").hide();

var request = indexedDB.open("files", 1);
request.onsuccess = function(e) {
    $("#uploadFile").show();
    if(hasMicPermission) {
        $("#recordMicButton").show();
    }
    idb = e.target.result;
    loadQueue.forEach(function(f) {
        fileReloadAudio(f);
    });
    loadQueue = [];
};

request.onupgradeneeded = function(e) { 
    e.target.result.createObjectStore("sounds", { autoIncrement: true });
};

function fileSelected() {
    var reader = new FileReader();
    var file = $("#uploadFile")[0].files[0];
    $("#fileUploadingWrapper").show();
    reader.onload = function() {
        var arrayBuffer = this.result;
        
        idb.transaction("sounds", "readwrite").objectStore("sounds").add(arrayBuffer).onsuccess = function(e) {
            var fileId = e.target.result;
            $("#sourceSelect").val("file:" + fileId + "/" + file.name);
            filesToUnload.push(fileId);
            $("#fileUploadingWrapper").hide();
            $("#uploadFile")[0].parentNode.innerHTML = $("#uploadFile")[0].parentNode.innerHTML;
            inputChange(false);
        };
    };
    reader.readAsArrayBuffer(file);
}

function fileReloadAudio(charNum) {
    if(!idb) {
        loadQueue.push(charNum);
        return;
    }
    setError(charNum, 1);
    updateReloadButton(true);
    var fileId = parseInt(bindings[charNum].source.split(":")[1].split("/")[0]);
    var t = idb.transaction("sounds").objectStore("sounds").get(fileId);
    t.onsuccess = function(e) {
        if(e.target.result === undefined) {
            loadError(charNum, bindings[charNum].source.split(":")[1].split("/").slice(-1)[0]);
            return;
        }
        herbert = e.target.result;
        ctx.decodeAudioData(e.target.result, function(buffer) {
            if(bindings[charNum] === undefined) {
                return;
            }
            bindings[charNum].audio = buffer;
            showWaveform();
            calculateSync(bindings[charNum].audio, bindings[charNum].mode == "song");
            setError(charNum, 0);
            updateReloadButton(false);
        }, function() {
            loadError(charNum, bindings[charNum].source.split(":")[1].split("/").slice(-1)[0]);
            updateReloadButton(false);
        });
    };
    t.onerror = function() {
        loadError(charNum, bindings[charNum].source.split(":")[1].split("/").slice(-1)[0]);
        updateReloadButton(false);
    };
}

function unloadIDBFiles(charNum) {
    var currId;
    if($("#sourceSelect").val().indexOf("file:") === 0) {
        currId = parseInt($("#sourceSelect").val().split(":")[1].split("/")[0]);
    }
    if(bindings[charNum].source && bindings[charNum].source.indexOf("file:") === 0) {
        filesToUnload.push(parseInt(bindings[charNum].source.split(":")[1].split("/")[0]));
    }
    filesToUnload.forEach(function(f) {
        if(f != currId) {
            idb.transaction("sounds", "readwrite").objectStore("sounds").delete(f);
        }
    });
    filesToUnload = [];
}
