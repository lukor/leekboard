function settings() {
    $("#globalSettingsWrapper").show();
    mode = "globalSettings";
}

function cancelSelect() {
    if(mode == "aliasTarget") {
        setAliasTarget(curSettings);
    }
    if(mode == "seqEvent") {
        selectedEventTarget(undefined);
    }
    if(sequenceRecording) {
        finishRecording();
    }
}
function changeSettings(charCode) {
    if(sequenceRecording) {
        return;
    }
    if(mode == "aliasTarget") {
        setAliasTarget(charCode);
        return;
    }
    if(mode == "seqEvent") {
        selectedEventTarget(charCode);
        return;
    }
    for(var i = 0; i < sounds[charCode].length; i++) {
        sounds[charCode][i].stop();
    }
    setPlaying(charCode, 0);
    if(dbClient.isAuthenticated()) {
        $("#selectFromDB").show();
    } else {
        $("#selectFromDB").hide();
    }
    if(oauthToken !== undefined) {
        $("#selectFromGD").show();
    } else {
        $("#selectFromGD").hide();
    }
    $("#settingsWrapper").show();
    curSettings = charCode;
    reloadSequenceList();
    $("#modeSelect").val(bindings[curSettings].mode);
    $("#sourceSelect").val(bindings[curSettings].source);
    $("#finishAfterUpCB").prop("checked", bindings[curSettings].finish);
    $("#slider").slider("values", 0, bindings[curSettings].start * 100);
    $("#slider").slider("values", 1, bindings[curSettings].stop * 100);
    $("#volSlider").slider("values", 0, bindings[curSettings].volume);
    $("#pitchSlider").slider("values", 0, bindings[curSettings].pitch);
    $("#timeSlider").slider("values", 0, bindings[curSettings].deviation);
    $("#bpmSelect").val(bindings[curSettings].bpm || 90);
    $("#beatsSelect").val(bindings[curSettings].beats || 1);
    $("#decayTime").val(bindings[curSettings].decay);
    $("#buttonSequence").val(bindings[curSettings].sequence);
    $("#aliasForButton > span").html(bindings[curSettings].forAlias === undefined ? "None" : getButtonName(bindings[curSettings].forAlias));
    mode = "settings";
    soundNeedsReload = false;
    updateReloadButton(false);
    showWaveform();
    modeChange();
}
function hideSettings(e, reload) {
    if(dbSettings) {
        dbSettingsCancel();
        return;
    }
    $(".frontWrapper").hide();
    if(mode == "settings" && reload === undefined) {
        unloadIDBFiles(curSettings);
        bindings[curSettings].source = $("#sourceSelect").val();
        bindings[curSettings].finish = $("#finishAfterUpCB").prop("checked");
        bindings[curSettings].start = $("#slider").slider("values", 0) / 100;
        bindings[curSettings].stop = $("#slider").slider("values", 1) / 100;
        bindings[curSettings].volume = $("#volSlider").slider("values", 0);
        bindings[curSettings].pitch = $("#pitchSlider").slider("values", 0);
        bindings[curSettings].deviation = $("#timeSlider").slider("values", 0);
        bindings[curSettings].bpm = Math.abs(parseFloat($("#bpmSelect").val())) || 90;
        bindings[curSettings].beats = Math.abs(parseInt($("#beatsSelect").val())) || 1;
        bindings[curSettings].decay = Math.abs(parseFloat($("#decayTime").val()));
        bindings[curSettings].sequence = $("#buttonSequence").val();
        if(soundNeedsReload) {
            reloadAudio(curSettings);
        }
        if(isMicRecording) {
            isMicRecording = 0;
            micRecorder.stop();
            $("#micRecordIcon").animate({
                "border-radius": "+=50%"
            });
            micStream.getAudioTracks().forEach(function(track) {
                track.stop();
            });
        }
        editButton(curSettings, buttonTextFromBinding(bindings[curSettings]));
        if(sounds[curSettings] !== undefined) {
            for(var i = 0; i < sounds[curSettings].length; i++) {
                sounds[curSettings][i].stop();
            }
        }
        sounds[curSettings] = [];
        saveBindings();
    } else if(mode == "globalSettings") {
        changeGlobalSettings();
    }
    if(soundPreview !== undefined) {
        soundPreview.stop();
        soundPreview = undefined;
    }
    mode = "play";
}

function changeGlobalSettings() {
    sounds.forEach(function(e, code) {
        setPlaying(code, 0);
        e.forEach(function(sound) {
            sound.stop();
        });
    });
    localStorage.setItem("midiInput", $("#midiInputs").val());
    if($("#pianoMode").prop("checked")) {
        $("div.mainKey").show();
    } else {
        $("div.mainKey").hide();
    }
    localStorage.setItem("pianoMode", $("#pianoMode").prop("checked"));
    localStorage.setItem("autoReload", $("#autoReload").prop("checked"));
    localStorage.setItem("livePreview", $("#livePreview").prop("checked"));
    $('[id^=btn]').remove();
    loadButtons();
    loadLayout();
}

$("body").ready(function() {
    if(localStorage.getItem("livePreview") === null) {
        localStorage.setItem("livePreview", true);
    }
    $("#pianoMode").prop("checked", localStorage.getItem("pianoMode") == "true");
    $("#autoReload").prop("checked", localStorage.getItem("autoReload") == "true");
    $("#livePreview").prop("checked", localStorage.getItem("livePreview") == "true");
});
