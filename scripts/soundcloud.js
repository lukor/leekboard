function scReloadAudio(charNum) {
    bindings[charNum].audio = undefined;
    setError(charNum, 1);
    updateReloadButton(true);
    var trackUrl = 'https://api.soundcloud.com/resolve?url=' + bindings[charNum].source + '&client_id=0O00O2LQYqOTBUo02kmTSi8PdOIbuoQU&format=json';
    
    $.getJSON(trackUrl, function(data) {
        var url = new URL(data.stream_url + '?client_id=0O00O2LQYqOTBUo02kmTSi8PdOIbuoQU');
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.responseType = "arraybuffer";
        request.onload = function() {
            ctx.decodeAudioData(request.response, function(buffer) {
                if(bindings[charNum] === undefined) {
                    return;
                }
                bindings[charNum].audio = buffer;
                showWaveform();
                calculateSync(bindings[charNum].audio, bindings[charNum].mode == "song");
                setError(charNum, 0);
                updateReloadButton(false);
            }, function() {
                loadError(charNum, bindings[charNum].source);
                updateReloadButton(false);
            });
        };
        request.send();
    });
}
