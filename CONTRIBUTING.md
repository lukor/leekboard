# Contributing to Leekboard

At the moment, external contribution to Leekboard is not planned as I want to finish my plans for this project first. Therefore, please refrain from submitting pull requests. This might change in the future.

However, if you have an idea which could improve the project, feel free to submit an issue on GitLab.