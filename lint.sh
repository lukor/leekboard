#!/bin/bash
# HTML
cat .html.lint | while read file; do echo Processing "$file"; tidy -errors -q $file; done

# CSS
cat .css.lint | xargs csslint --errors=box-sizing,bulletproof-font-face,compatible-vendor-prefixes,empty-rules,display-property-grouping,duplicate-background-images,duplicate-properties,fallback-colors,floats,font-faces,font-sizes,gradients,ids,import,important,known-properties,outline-none,overqualified-elements,qualified-headings,regex-selectors,shorthand,star-property-hack,text-indent,underscore-property-hack,unique-headings,universal-selector,unqualified-attributes,vendor-prefix,zero-units --ignore=adjoining-classes,order-alphabetical,box-model

# JavaScript
cat .js.lint | xargs jshint
